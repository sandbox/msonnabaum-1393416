<?php

class DrupalRedisCacheTagsPlugin extends Redis_Cache_PhpRedis {
  function getTagKey($tag) {
    return $this->prefix . ':cachetags:' . $tag;
  }

  function set($cid, $data, $expire = CACHE_PERMANENT, $tags = array()) {
    $client = Redis_Client::getClient();
    $client->multi(Redis::PIPELINE);
    $key = $this->getKey($cid) . ':data';
    foreach (DrupalCacheTags::flatten($tags) as $tag) {
      $tag_key = $this->getTagKey($tag);
      $client->sAdd($tag_key, $key);
    }
    $client->exec();

    parent::set($cid, $data, $expire = CACHE_PERMANENT);

  }

  function get($cid) {
    $client     = Redis_Client::getClient();
    $key        = $this->getKey($cid);

    list($serialized, $data) = $client->mget(array($key . ':serialized', $key . ':data'));

    if (FALSE === $data) {
      return FALSE;
    }

    $cached          = new stdClass;
    $cached->data    = $serialized ? unserialize($data) : $data;
    $cached->expires = 0; // FIXME: Redis does not seem to allow us to fetch
                          // expire value. The only solution would be to create
                          // a new key. Who on earth need this value anyway?

    return $cached;
  }

  function invalidateTags($tags) {
    $client = Redis_Client::getClient();
    $keys = array();
    foreach (DrupalCacheTags::flatten($tags) as $tag) {
      $keys[] = $this->getTagKey($tag);
    }
    // Use call_user_func_array here since sUnion unfortunately doesn't take an
    // array as an argument.
    $cids = call_user_func_array(array($client, "sUnion"), $keys);
    $client->del($cids);
  }
}
