<?php

/**
 * Interface for cache backends that support tags.
 *
 * Each cache backend that supports tags should implement this interface.
 *
 * @see cache_invalidate()
 * @see DrupalDatabaseCache
 */
interface DrupalCacheTagsInterface {
  /**
   * Invalidate each tag in the $tags array.
   *
   * @param $tags
   *   Associative array of tags, in the same format that is passed to
   *   DrupalCacheInterface::set().
   *
   * @see DrupalCacheInterface::set()
   */
  function invalidateTags($tags);
}

/**
 * Base abstract class for cache tags implementations.
 *
 * Provides default implementations and helper methods.
 */
abstract class DrupalCacheTags implements DrupalCacheTagsInterface {
  function isValid($checksum, $tags) {
    return $checksum == $this->checksum($tags);
  }

  /**
   * Flatten a tags array into a numeric array suitable for string storage.
   *
   * @param $tags
   *   Associative array of tags to flatten.
   *
   * @return
   *   Numeric array of flattened tag identifiers.
   */
  static function flatten($tags) {
    if (isset($tags[0])) return $tags;
    $flat_tags = array();
    foreach ($tags as $namespace => $values) {
      if (is_array($values)) {
        foreach ($values as $value) {
          $flat_tags[] = "$namespace:$value";
        }
      }
      else {
        $flat_tags[] = "$namespace:$values";
      }
    }
    return $flat_tags;
  }
}

/**
 * Invalidate each tag in the $tags array.
 *
 * Procedural (convenience) wrapper for the cache_tags()->invalidate() method.
 * Cache entries tagged with any one of these will subsequently return
 * FALSE for cache_get().
 *
 * @param $tags
 *   Associative array of tags, in the same format that is passed to cache_set().
 *
 * @see cache_set()
 */
function cache_invalidate($tags) {
  $classes = cache_get_classes();
  foreach ($classes as $bin => $class) {
    if (in_array("DrupalCacheTagsInterface", class_implements($class))) {
      _cache_get_object($bin)->invalidateTags($tags);
    }
  }
}


/**
 * Helper function to add cache tags for the current request.
 */
function cache_add_request_tags($tags = array()) {
  $stored_tags = &drupal_static(__FUNCTION__, array());
  if (!empty($tags)) {
    // Merge cache tags into appropriate namespace.
    foreach ($tags as $namespace => $values) {
      if (!isset($stored_tags[$namespace])) {
        $stored_tags[$namespace] = array();
      }
      foreach ($values as $value) {
        if (!in_array($value, $stored_tags[$namespace])) {
          $stored_tags[$namespace][] = $value;
        }
      }
    }
  }
  return $stored_tags;
}


/**
 * Helper function to get cache tags for the current page.
 */
function cache_get_request_tags() {
  return cache_add_request_tags();
}


/**
 * Pretty ghetto, but the best I could come up with in D7, and reasonably fast.
 */
function cache_get_classes() {
  global $conf;
  $classes = array();
  foreach ($conf as $key => $value) {
    if (strpos($key, "cache_class_") === 0) {
      $bin = substr($key, 12);
      $classes[$bin] = $value;
    }
  }
  return $classes;
}
